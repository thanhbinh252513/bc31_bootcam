import React from "react";
import { Col, Form, Input, message, Row } from "antd";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { userService } from "../services/user.service";
import { setUserInfor } from "../redux/slice/userSlice";
import { localStorageServ } from "./../services/localStorageService";

export default function LoginPage() {
  let dispatch = useDispatch();
  let history = useNavigate();
  const onFinish = (values) => {
    console.log("Success:", values);
    userService
      .postLogin(values)
      .then((res) => {
        console.log(res);
        dispatch(setUserInfor(res.data.content));
        localStorageServ.user.set(res.data.content);
        setTimeout(() => {
          history("/user-management");
        }, 1000);
      })
      .catch((err) => {
        console.log();
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="bg-red-400 h-screen w-screen p-10">
      <div className="container mx-auto bg-white p-10 rounded-xl">
        <Row gutter={16}>
          {" "}
          <Col className="gutter-row" span={10}>
            <Form
              name="basic"
              layout="vertical"
              labelCol={{
                span: 8,
              }}
              wrapperCol={{
                span: 24,
              }}
              initialValues={{
                remember: true,
              }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                label={<p className="font-medium text-blue-700">Tài khoản</p>}
                name="taiKhoan"
                rules={[
                  {
                    required: true,
                    message: "vui lòng nhập tài khoản",
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                label="Mật khẩu"
                name="matKhau"
                rules={[
                  {
                    required: true,
                    message: "vui lòng nhập mật khẩu",
                  },
                ]}
              >
                <Input.Password />
              </Form.Item>
              <div className="flex justify-center">
                <button className="rounded px-5 py-2 text-white bg-red-500">
                  đăng nhập
                </button>
              </div>
            </Form>
          </Col>
        </Row>
      </div>
    </div>
  );
}
