import logo from "./logo.svg";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
// import LoginPage from "./LoginPage/LoginPage";
// import UserManagementPage from "./UserManagementPage/UserManagementPage";
// import NotFoundPage from "./NotFoundPage/NotFoundPage";
import { adminRoute } from "./routes/adminRoute";
import Layout from "./Components/Layout/Layout";

function App() {
  let renderRoutes = () => {
    return adminRoute.map(({ path, Component }) => {
      return (
        <Route
          path={path}
          element={
            <Layout>
              <Component />
            </Layout>
          }
        />
      );
    });
  };
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>{renderRoutes()}</Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
