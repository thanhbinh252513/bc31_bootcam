import { createSlice } from "@reduxjs/toolkit";

const initialState = { user: null };

const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    setUserInfor: (state, { payload }) => {
      console.log(payload);
      state.user = payload;
    },
  },
});

export const { setUserInfor } = userSlice.actions;
export default userSlice.reducer;
