import React, { Component } from "react";
import UserTable from "../UserTable/UserTable";

export default class UserManagementPage extends Component {
  render() {
    return (
      <div>
        <UserTable />
      </div>
    );
  }
}
