import axios from "axios";
import { https, TOKEN_CYBER } from "./configURL";

export let movieService = {
  getMovieList: () => {
    return https.get(
      "https://movienew.cybersoft.edu.vn/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP05"
    );
  },
};
