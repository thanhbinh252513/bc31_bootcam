import axios from "axios";
import { https, TOKEN_CYBER } from "./configURL";
// import { TOKEN_CYBER } from "./movie.service";

export let userService = {
  postLogin: (loginData) => {
    return axios({
      url: "https://movienew.cybersoft.edu.vn/api/QuanLyNguoiDung/DangNhap",
      method: "POST",
      data: loginData,
      headers: {
        TokenCybersoft: TOKEN_CYBER,
      },
    });
  },
  deleteUser: (taiKhoan) => {
    return https.delete(
      `https://movienew.cybersoft.edu.vn/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${taiKhoan}`
    );
  },
};
