import LoginPage from "../LoginPage/LoginPage";
import NotFoundPage from "../NotFoundPage/NotFoundPage";
import UserManagementPage from "../UserManagementPage/UserManagementPage";

export const adminRoute = [
  {
    path: "/login",
    Component: LoginPage,
  },
  {
    path: "/",
    Component: UserManagementPage,
  },
  {
    path: "/user-management",
    Component: UserManagementPage,
  },
  {
    path: "*",
    Component: NotFoundPage,
  },
];
