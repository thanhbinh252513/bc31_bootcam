import React from "react";
import "./notFound.css";
export default function NotFoundPage() {
  return (
    <div>
      <div className="container-fluid notfound404">
        <h1 className="clip-text cover-404 animated galaxy faster">404</h1>
        <h2 className="animated fadeIn delay-05s">
          Fuck! There are many pages in the galaxy, but this one has not been
          found...
        </h2>
        <p className="animated fadeIn delay-1s">
          It looks like you entered an incorrect address. Check once again.
        </p>
      </div>
    </div>
  );
}
