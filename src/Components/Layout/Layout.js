import React, { Component } from "react";
import HeaderTheme from "../HeaderTheme/HeaderTheme";

export default function Layout({ children }) {
  return (
    <div>
      <HeaderTheme />
      {children}
      {/* mặt định root ở giữa */}
    </div>
  );
}
