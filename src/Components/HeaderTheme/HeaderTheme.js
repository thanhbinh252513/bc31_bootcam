import React from "react";
import { DesktopResponsive } from "./../../HOC/ResponsiveComponent";
import Header_Desktop from "./Header_Desktop";
import Header_Tablet from "./Header_Tablet";
import Header_Mobile from "./Header_Mobile";
export default function HeaderTheme() {
  return (
    <>
      <DesktopResponsive>
        <Header_Desktop />
      </DesktopResponsive>
      <DesktopResponsive>
        <Header_Mobile />
      </DesktopResponsive>
      <DesktopResponsive>
        <Header_Tablet />
      </DesktopResponsive>
    </>
  );
}
