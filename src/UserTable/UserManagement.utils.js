import { Space, Table, Tag } from "antd";
export const columns = [
  {
    title: "Tài khoản",
    dataIndex: "taiKhoan",
    key: "taiKhoan",
  },
  {
    title: "Họ tên",
    dataIndex: "hoTen",
    key: "hoTen",
  },
  {
    title: "Email",
    dataIndex: "email",
    key: "email",
  },
  {
    title: "Loại khách hàng",
    key: "maLoaiNguoiDung",
    dataIndex: "tagmaLoaiNguoiDungs",
    render: (text) => {
      if (text == "QuanTri") {
        return <Tag color={"red"}>Quản trị</Tag>;
      } else {
        return <Tag color={"blue"}>Khách hàng</Tag>;
      }
    },
  },
  {
    title: "thao tác",
    dataIndex: "action",
    key: "action",
  },
];
//     render: (_, { tags }) => (
//       <>
//         {tags.map((tag) => {
//           let color = tag.length > 5 ? "geekblue" : "green";

//           if (tag === "loser") {
//             color = "volcano";
//           }

//           return (
//             <Tag color={color} key={tag}>
//               {tag.toUpperCase()}
//             </Tag>
//           );
//         })}
//       </>
//     ),
//   },
//   {
//     title: "Action",
//     key: "action",
//     render: (_, record) => (
//       <Space size="middle">
//         <a>Invite {record.name}</a>
//         <a>Delete</a>
//       </Space>
//     ),
//   },
// ];
