import { Space, Table, Tag } from "antd";
import React, { useEffect, useState } from "react";
import { movieService } from "../services/movie.service";
import { columns } from "./UserManagement.utils";
import UserAction from "./UserAction";

// const data = [
//   {
//     key: "1",
//     name: "John Brown",
//     age: 32,
//     address: "New York No. 1 Lake Park",
//     tags: ["nice", "developer"],
//   },
//   {
//     key: "2",
//     name: "Jim Green",
//     age: 42,
//     address: "London No. 1 Lake Park",
//     tags: ["loser"],
//   },
//   {
//     key: "3",
//     name: "Joe Black",
//     age: 32,
//     address: "Sidney No. 1 Lake Park",
//     tags: ["cool", "teacher"],
//   },
// ];

const UserTable = () => {
  const [dataUser, setDataUser] = useState([]);

  useEffect(() => {
    let fetchListUser = () => {
      movieService
        .getMovieList()
        .then((res) => {
          let userList = res.content.map((user) => {
            return {
              ...user,
              action: (
                <UserAction
                  onSuccess={fetchListUser}
                  taiKhoan={user.taiKhoan}
                />
              ),
            };
          });
          console.log(userList);
          setDataUser(userList);
        })
        .catch((err) => {
          console.log(err);
        });
    };
    fetchListUser();
  }, []);
  return <Table columns={columns} dataSource={dataUser} />;
};

export default UserTable;
